//
//  ViewController.swift
//  Calculator
//
//  Created by Admin on 06.03.17.
//  Copyright © 2017 thecomsoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var resultView: UITextView!
    var var1: String = "";
    var var2: String = "";
    var oper: String = "";
    var first: Bool = true;

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addNumber(data: String) {
        if first {
            var1.append(data);
        }
        else{
            var2.append(data);
        }
        resultView.text.append(data);
    }
    func addMethod(data: String){
        oper = data;
        first = false;
        resultView.text.append(data);
    }
    func calculate(){
        var res = "";
        resultView.text.append("=");
        switch oper {
        case "+":
            res = String(Float(var1)! + Float(var2)!);
            break;
        case "-":
            res = String(Float(var1)! - Float(var2)!);
            break;
        case "*":
            res = String(Float(var1)! * Float(var2)!);
            break;
        case "/":
            res = String(Float(var1)! / Float(var2)!);
            break;
        default: break
        }
        
        var1 = "";
        var2 = "";
        first = true;
        
        resultView.text.append(res);
        resultView.text.append("\n");
    }
    

    @IBAction func button9Action(_ sender: Any) {
        addNumber(data: "9");
    }
    @IBAction func button8Action(_ sender: Any) {
        addNumber(data: "8");
    }
    @IBAction func button7Action(_ sender: Any) {
        addNumber(data: "7");
    }
    @IBAction func button6Action(_ sender: Any) {
        addNumber(data: "6");
    }
    @IBAction func button5Action(_ sender: Any) {
        addNumber(data: "5");
    }
    @IBAction func button4Action(_ sender: Any) {
        addNumber(data: "4");
    }
    @IBAction func button3Action(_ sender: Any) {
        addNumber(data: "3");
    }
    @IBAction func button2Action(_ sender: Any) {
        addNumber(data: "2");
    }
    @IBAction func button1Action(_ sender: Any) {
        addNumber(data: "1");
    }
    @IBAction func button0Action(_ sender: Any) {
        addNumber(data: "0");
    }
    @IBAction func buttonComaAction(_ sender: Any) {
        addNumber(data: ".");
    }
    @IBAction func buttonEqualsAction(_ sender: Any) {
        calculate();
    }
    @IBAction func buttonPlusAction(_ sender: Any) {
        addMethod(data: "+");
    }
    @IBAction func buttonMinusAction(_ sender: Any) {
        addMethod(data: "-");
    }
    @IBAction func buttonAndAction(_ sender: Any) {
        addMethod(data: "*");
    }
    @IBAction func buttonShareAction(_ sender: Any) {
        addMethod(data: "/");
    }

}

